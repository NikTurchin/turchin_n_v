автор: Турчин Н.В.
дата: 06.05.2020

### Задание
1. Подобрать и зачитать небольшое изображение $S$ в градациях серого.
2. Построить и нарисовать гистограмму $H_s$ распределения яркости пикселей исходного изображения.
3. Сгенерировать табличную функцию преобразования яркости. Построить график $V$ табличной функции преобразования яркости.
4. Применить табличную функцию преобразования яркости к исходному изображению и получить $L$, нарисовать гистограмму $H_L$ преобразованного изображения.
5. Применить CLAHE с тремя разными наборами параметров (визуализировать обработанные изображения $C_i$ и их гистограммы $H_{C_{i}}$).
6. Реализовать глобальный метод бинаризации (подобрать порог по гистограмме, применить пороговую бинаризацию). Визуализировать на одном изображении исходное $S$ и бинаризованное $B_G$ изображения.
7. Реализовать метод локальной бинаризации. Визуализировать на одном изображении исходное $S$ и бинаризованное $B_L$ изображения.
8. Улучшить одну из бинаризаций путем применения морфологических фильтров. Визуализировать на одном изображении бинарное изображение до и после фильтрации $M$.
9. Сделать визуализацию $K$ бинарной маски после морфологических фильтров поверх исходного изображения (могут помочь подсветка цветом и альфа-блендинг).


### Результаты

![](./images/lab03.src.png)
Рис. 1. Исходное полутоновое изображение $S$

![](./images/lab03.hist.src.png)
Рис. 2. Гистограмма $H_s$ исходного полутонового изображения $S$

![](./images/lab03.lut.png)
Рис. 3. Визуализация функции преобразования $V$

![](./images/lab03.lut.src.png)
Рис. 4.1. Таблично пребразованное изображение $L$

![](./images/lab03.hist.lut.src.png)
Рис. 4.2. Гистограмма $H_L$ таблично-пребразованного изображения $L$

![](./images/lab03.clahe.1.png)
Рис. 5.1. Преобразование $C_1$ CLAHE с параметрами Size(8, 8)

![](./images/lab03.hist.clahe.1.png)
Рис. 5.2. Гистограмма $H_{C_{1}}$

![](./images/lab03.clahe.2.png)
Рис. 5.3. Преобразование $C_2$ CLAHE с параметрами Size(2, 8)

![](./images/lab03.hist.clahe.2.png)
Рис. 5.4. Гистограмма $H_{C_{2}}$

![](./images/lab03.clahe.3.png)
Рис. 5.5. Преобразование $C_3$ CLAHE с параметрами Size(8, 2)

![](./images/lab03.hist.clahe.3.png)
Рис. 5.6. Гистограмма $H_{C_{3}}$

![](./images/lab03.bin.global.png)
Рис. 6. Изображение $S$ до и $B_G$ после глобальной бинаризации

![](./images/lab03.bin.local.png)
Рис. 7. Изображение $S$ до и $B_L$ после локальной бинаризации

![](./images/lab03.morph.png)
Рис. 8. До и после морфологической фильтрации $M$

![](./images/lab03.mask.png)
Рис. 9. Визуализация маски $K$

### Текст программы

```cpp
#include <vector>
#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int brightness_function(int x) {
	if (x <= 50)
	{
		return(5 * x);
	}
	if ((x > 50) && (x < 100))
	{
		return(-5 * (x - 50) + 250);
	}
	if (x >= 100)
	{
		return(x - 100);
	}
}

void draw_image_histogram(Mat gray_image, int hist_size, string filename) {
	
	array<int, 256> hist{ 0 };
	for (int j_row = 0; j_row < gray_image.rows; j_row++) {
		for (int i_col = 0; i_col < gray_image.cols; i_col++) {
			hist[gray_image.at<uint8_t>(j_row, i_col)] += 1; 
		}
	}

	
	int max = 0;
	for (int i = 0; i < 256; i++)
	{
		if (max < hist[i])
		{
			max = hist[i];
		}
	}

	
	float y_scale = (1.0 * hist_size / max); 
	Mat histogram(hist_size, 256, CV_8UC1, Scalar(255, 0, 0));
	for (int x = 0; x < 256; x++) {
		line(histogram, Point(x, 0), Point(x, hist_size - y_scale * hist[x]), Scalar(10, 255, 255), 1); 
	}

	imwrite(filename, histogram);
}

int main()
{
	String path = ".\\images\\";
	Mat initial_rgb_image = imread(path + "cross.jpg");
	Mat gray_image;
	cvtColor(initial_rgb_image, gray_image, COLOR_BGR2GRAY);
	imwrite(path + "lab03.src.png", gray_image);

	draw_image_histogram(gray_image, 150, path + "lab03.hist.src.png");

	Mat brightness(256, 256, CV_8UC1, Scalar(255, 0, 0));
	for (int x = 1; x < brightness.cols; x++) {
		line(brightness, Point(x - 1, 255 - brightness_function(x - 1)), Point(x, 255 - brightness_function(x)), (1));
	}
	imwrite(path + "lab03.lut.png", brightness);

	Mat image_with_brigtness_function;
	gray_image.copyTo(image_with_brigtness_function);
	for (int j_row = 0; j_row < gray_image.rows; j_row++) {
		for (int i_col = 0; i_col < gray_image.cols; i_col++) {
			image_with_brigtness_function.at<uint8_t>(j_row, i_col) =
				brightness_function(image_with_brigtness_function.at<uint8_t>(j_row, i_col));
		}
	}
	imwrite(path + "lab03.lut.src.png", image_with_brigtness_function);
	draw_image_histogram(image_with_brigtness_function, 150, path + "lab03.hist.lut.src.png");


	Mat pic_clahe1, pic_clahe2, pic_clahe3;
	createCLAHE(10, Size(8, 8))->apply(gray_image, pic_clahe1);
	createCLAHE(10, Size(2, 8))->apply(gray_image, pic_clahe2);
	createCLAHE(10, Size(8, 2))->apply(gray_image, pic_clahe3);
	imwrite(path + "lab03.clahe.1.png", pic_clahe1);
	imwrite(path + "lab03.clahe.2.png", pic_clahe2);
	imwrite(path + "lab03.clahe.3.png", pic_clahe3);
	draw_image_histogram(pic_clahe1, 150, path + "lab03.hist.clahe.1.png");
	draw_image_histogram(pic_clahe2, 150, path + "lab03.hist.clahe.2.png");
	draw_image_histogram(pic_clahe3, 150, path + "lab03.hist.clahe.3.png");


	Mat global_binarization_image;
	cv::threshold(gray_image, global_binarization_image, 0, 255, THRESH_BINARY | THRESH_OTSU);
	Mat full_global_binarization;
	full_global_binarization.push_back(gray_image);
	full_global_binarization.push_back(global_binarization_image);
	imwrite(path + "lab03.bin.global.png", full_global_binarization);


	Mat local_binarization_image;
	cv::adaptiveThreshold(gray_image, local_binarization_image, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 3, 2);
	Mat full_local_binarization;
	full_local_binarization.push_back(gray_image);
	full_local_binarization.push_back(local_binarization_image);
	imwrite(path + "lab03.bin.local.png", full_local_binarization);


	Mat morphology_image;
	Mat full_morphology_image;
	Mat element = cv::getStructuringElement(MORPH_RECT, Size(8, 8));
	cv::morphologyEx(global_binarization_image, morphology_image, MORPH_OPEN, element);
	full_morphology_image.push_back(global_binarization_image);
	full_morphology_image.push_back(morphology_image);
	imwrite(path + "lab03.morph.png", full_morphology_image);

	Mat binary_mask;
	double alpha = 0.7; double beta = (1.0 - alpha);
	addWeighted(global_binarization_image, alpha, gray_image, beta, 0.0, binary_mask);
	imwrite(path + "lab03.mask.png", binary_mask);

	waitKey(0);
	return 0;
}
```
