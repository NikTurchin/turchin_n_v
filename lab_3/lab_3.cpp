#include <vector>
#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int brightness_function(int x) {
	if (x <= 50)
	{
		return(5 * x);
	}
	if ((x > 50) && (x < 100))
	{
		return(-5 * (x - 50) + 250);
	}
	if (x >= 100)
	{
		return(x - 100);
	}
}

void draw_image_histogram(Mat gray_image, int hist_size, string filename) {
	
	array<int, 256> hist{ 0 };
	for (int j_row = 0; j_row < gray_image.rows; j_row++) {
		for (int i_col = 0; i_col < gray_image.cols; i_col++) {
			hist[gray_image.at<uint8_t>(j_row, i_col)] += 1; 
		}
	}

	
	int max = 0;
	for (int i = 0; i < 256; i++)
	{
		if (max < hist[i])
		{
			max = hist[i];
		}
	}

	
	float y_scale = (1.0 * hist_size / max); 
	Mat histogram(hist_size, 256, CV_8UC1, Scalar(255, 0, 0));
	for (int x = 0; x < 256; x++) {
		line(histogram, Point(x, 0), Point(x, hist_size - y_scale * hist[x]), Scalar(10, 255, 255), 1); 
	}

	imwrite(filename, histogram);
}

int main()
{
	String path = ".\\images\\";
	Mat initial_rgb_image = imread(path + "cross.jpg");
	Mat gray_image;
	cvtColor(initial_rgb_image, gray_image, COLOR_BGR2GRAY);
	imwrite(path + "lab03.src.png", gray_image);

	draw_image_histogram(gray_image, 150, path + "lab03.hist.src.png");

	Mat brightness(256, 256, CV_8UC1, Scalar(255, 0, 0));
	for (int x = 1; x < brightness.cols; x++) {
		line(brightness, Point(x - 1, 255 - brightness_function(x - 1)), Point(x, 255 - brightness_function(x)), (1));
	}
	imwrite(path + "lab03.lut.png", brightness);

	Mat image_with_brigtness_function;
	gray_image.copyTo(image_with_brigtness_function);
	for (int j_row = 0; j_row < gray_image.rows; j_row++) {
		for (int i_col = 0; i_col < gray_image.cols; i_col++) {
			image_with_brigtness_function.at<uint8_t>(j_row, i_col) =
				brightness_function(image_with_brigtness_function.at<uint8_t>(j_row, i_col));
		}
	}	imwrite(path + "lab03.lut.src.png", image_with_brigtness_function);
	draw_image_histogram(image_with_brigtness_function, 150, path + "lab03.hist.lut.src.png");


	Mat pic_clahe1, pic_clahe2, pic_clahe3;
	createCLAHE(10, Size(8, 8))->apply(gray_image, pic_clahe1);
	createCLAHE(10, Size(2, 8))->apply(gray_image, pic_clahe2);
	createCLAHE(10, Size(8, 2))->apply(gray_image, pic_clahe3);
	imwrite(path + "lab03.clahe.1.png", pic_clahe1);
	imwrite(path + "lab03.clahe.2.png", pic_clahe2);
	imwrite(path + "lab03.clahe.3.png", pic_clahe3);
	draw_image_histogram(pic_clahe1, 150, path + "lab03.hist.clahe.1.png");
	draw_image_histogram(pic_clahe2, 150, path + "lab03.hist.clahe.2.png");
	draw_image_histogram(pic_clahe3, 150, path + "lab03.hist.clahe.3.png");


	Mat global_binarization_image;
	cv::threshold(gray_image, global_binarization_image, 0, 255, THRESH_BINARY | THRESH_OTSU);
	Mat full_global_binarization;
	full_global_binarization.push_back(gray_image);
	full_global_binarization.push_back(global_binarization_image);
	imwrite(path + "lab03.bin.global.png", full_global_binarization);


	Mat local_binarization_image;
	cv::adaptiveThreshold(gray_image, local_binarization_image, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 3, 2);
	Mat full_local_binarization;
	full_local_binarization.push_back(gray_image);
	full_local_binarization.push_back(local_binarization_image);
	imwrite(path + "lab03.bin.local.png", full_local_binarization);


	Mat morphology_image;
	Mat full_morphology_image;
	Mat element = cv::getStructuringElement(MORPH_RECT, Size(8, 8));
	cv::morphologyEx(global_binarization_image, morphology_image, MORPH_OPEN, element);
	full_morphology_image.push_back(global_binarization_image);
	full_morphology_image.push_back(morphology_image);
	imwrite(path + "lab03.morph.png", full_morphology_image);

	Mat binary_mask;
	double alpha = 0.7; double beta = (1.0 - alpha);
	addWeighted(global_binarization_image, alpha, gray_image, beta, 0.0, binary_mask);
	imwrite(path + "lab03.mask.png", binary_mask);

	waitKey(0);
	return 0;
}