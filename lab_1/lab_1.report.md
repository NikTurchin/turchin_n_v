## Работа 1. Исследование гамма-коррекции
автор: Турчин Н.В.
дата: @time_stemp

<!-- url:https://gitlab.com/NikTurchin/turchin_n_v -->

### Задание
1. Сгенерировать серое тестовое изображение $I_1$ в виде прямоугольника размером 768х60 пикселя с плавным изменение пикселей от черного к белому, одна градация серого занимает 3 пикселя по горизонтали.
2. Применить  к изображению $I_1$ гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение $G_1$.
3. Сгенерировать серое тестовое изображение $I_2$ в виде прямоугольника размером 768х60 пикселя со ступенчатым изменением яркости от черного к белому (от уровня 5 с шагом 10), одна градация серого занимает 30 пикселя по горизонтали.
4. Применить  к изображению $I_2$ гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение $G_2$.
5. Показать визуализацию результатов в виде одного изображения, об

### Результаты

![](G_1.png)
![](G_2.png)

Рис. 1. Результаты работы программы (сверху вниз $I_1$, $G_1$, $I_2$, $G_2$)

### Текст программы

```cpp
#include <opencv2/opencv.hpp>

using namespace cv;

int task1() {
	
	Mat full_image(120, 768, CV_8UC1, Scalar(255));
	
	int w = 768; 
	int h = 60;

	Mat roi_1 = full_image(Rect(0, 0, w, h));

	Mat& img(roi_1);
	
	int j = 0;
	for (int step = 0; step < img.cols; step++) {
		if (step % 3 == 0) {
			j++;
		}
		img.col(step).setTo(j);
	}

	Mat img_2;
	img.convertTo(img_2, CV_32FC1, 1.0 / 255);
	pow(img_2, 2.3, img_2);

	Mat roi_2 = full_image(Rect(0, h, w, h));
	img_2.convertTo(roi_2, CV_8UC1, 255.0);

	imwrite("G_1.png", full_image);

	waitKey(0);
	return 0;
}

int task2() {
	
	int h = 60;
	int w = 768;

	Mat img(Mat::zeros(h, w, CV_8UC1));
	Mat full_image(Size(w, 2 * h), CV_8UC1);

	for (int col = 0; col < w; ++col) {
		for (int row = 0; row < h; ++row) {
			img.at<uchar>(Point(col, row)) = 5 + col / 30 * 10;
		}
	}

	img.copyTo(full_image(Rect(0, 0, w, h)));
	img.convertTo(img, CV_32FC1, 1.0 / 255);
	cv::pow(img, 2.4, img);
	img.convertTo(img, CV_8UC1, 255);
	img.copyTo(full_image(Rect(0, h, w, h)));

	imwrite("G_2.png", full_image);
	waitKey(0);
	return 0;
}

int main()
{
	task1();
	task2();
}




```
