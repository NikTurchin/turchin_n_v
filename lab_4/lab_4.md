## Работа 4. Использование частных производных для выделения границ
Студент: Турчин Н.В., группа БПМ-16-2

### Задание
1. Сгенерировать серое тестовое изображение из квадратов и кругов с разными уровнями яркости (0, 127 и 255) так, чтобы присутствовали все сочетания.
2. Применить первый линейный фильтр и сделать визуализацию результата.
3. Применить второй линейный фильтр и сделать визуализацию результата.
4. Вычислить R=sqrt{F_1^2 + F_2^2}  и сделать визуализацию R.

### Результаты

![](https://a.radikal.ru/a18/2005/4c/d0dd9d17d196.png)
Рис. 1. Исходное тестовое изображение

![](https://b.radikal.ru/b27/2005/51/2ac1f9447c0e.png)
Рис. 2. Визуализация результата применения фильтра 1

![](https://d.radikal.ru/d02/2005/b7/f8746393ceca.png)
Рис. 3. Визуализация результата применения фильтра 2

![](https://d.radikal.ru/d33/2005/38/2b315a4630bd.png)
Рис. 4. Визуализация модуля градиента R

### Текст программы

```cpp

#include <windows.h>
#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main()
{
	int height = 100, width = 100;
	Mat result_image(Mat::zeros(height * 2, width * 3, CV_8UC1));

	Mat img(Mat::zeros(height, width, CV_8UC1));
	circle(img, Point(50, 50), 20, 255, -1, 8, 0);
	img.copyTo(result_image(Rect(0, 0, 100, 100)));
	for (int col = 0; col < width; col += 1)
	{
		for (int row = 0; row < height; row += 1)
		{
			img.at<uchar>(Point(col, row)) = 127;
		}
	}

	circle(img, Point(50, 50), 20, 0, -1, 8, 0);
	img.copyTo(result_image(Rect(0, 100, 100, 100)));

	circle(img, Point(50, 50), 20, 255, -1, 8, 0);
	img.copyTo(result_image(Rect(100, 0, 100, 100)));

	for (int col = 0; col < width; col += 1)
	{
		for (int row = 0; row < height; row += 1)
		{
			img.at<uchar>(Point(col, row)) = 255;
		}
	}

	circle(img, Point(50, 50), 20, 127, -1, 8, 0);
	img.copyTo(result_image(Rect(100, 100, 100, 100)));
	for (int col = 0; col < width; col += 1)
	{
		for (int row = 0; row < height; row += 1)
		{
			img.at<uchar>(Point(col, row)) = 127;
		}
	}
	circle(img, Point(50, 50), 20, 0, -1, 8, 0);
	img.copyTo(result_image(Rect(200, 100, 100, 100)));
	for (int col = 0; col < width; col += 1)
	{
		for (int row = 0; row < height; row += 1)
		{
			img.at<uchar>(Point(col, row)) = 0;
		}
	}

	circle(img, Point(50, 50), 20, 127, -1, 8, 0);
	img.copyTo(result_image(Rect(200, 0, 100, 100)));
	imshow("result", result_image);
	imwrite("lab_4.png", result_image);

	Mat fi_im1, fi_im2, fi_im3;

	Mat filt1(Mat::zeros(2, 2, CV_8SC1));
	Mat filt2(Mat::zeros(2, 2, CV_8SC1));
	filt1.at<int8_t>(0, 0) = 1.;
	filt1.at<int8_t>(1, 1) = -1.;
	filt2.at<int8_t>(0, 1) = 1.;
	filt2.at<int8_t>(1, 0) = -1.;

	filter2D(result_image, fi_im1, CV_32F, filt1, Point(-1, -1), 0, BORDER_REFLECT);
	filter2D(result_image, fi_im2, CV_32F, filt2, Point(-1, -1), 0, BORDER_REFLECT);

	imshow("operator 1", fi_im1);
	imshow("operator 2", fi_im2);
	imwrite("lab_4.operator1.png", fi_im1);
	imwrite("lab_4.operator2.png", fi_im2);

	pow(fi_im1, 2, fi_im1);
	pow(fi_im2, 2, fi_im2);
	sqrt((fi_im1 + fi_im2), fi_im3);
	fi_im3 = (fi_im3 + 255) / 2;
	fi_im3.convertTo(fi_im3, CV_8U);

	imshow("operator R", fi_im3);
	imwrite("lab_4.operatorR.png", fi_im3);

	waitKey(0);
	return 0;
}

```
